package com.yh.tb.schema.dao.master;

import com.yh.tb.schema.domain.TbSyncContentTable;

/**
* @author hbjav
* @description 针对表【tb_sync_content_table(记录同步内容详细)】的数据库操作Mapper
* @createDate 2023-01-16 16:43:17
* @Entity com.yh.tb.schema.domain.TbSyncContentTable
*/
public interface TbSyncContentTableMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TbSyncContentTable record);

    int insertSelective(TbSyncContentTable record);

    TbSyncContentTable selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TbSyncContentTable record);

    int updateByPrimaryKey(TbSyncContentTable record);

}
