package com.yh.tb.schema.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 记录微服务同步事务配置的内容信息
 * @TableName tb_sync_micserv_content
 */
public class TbSyncMicservContent implements Serializable {
    /**
     * 主键ID
     */
    private String id;

    /**
     * 同步事务ID
     */
    private String contentId;

    /**
     * 公司ID
     */
    private String compId;

    /**
     * 数据库url
     */
    private String dbUrl;

    /**
     * 数据库端口号
     */
    private String dbPort;

    /**
     * 数据库名称
     */
    private String dbName;

    /**
     * 数据库账户
     */
    private String dbUserName;

    /**
     * 数据库密码
     */
    private String dbPassword;

    /**
     * 数据库服务器账户
     */
    private String dbServUserName;

    /**
     * 数据库服务器密码
     */
    private String dbServPassword;

    /**
     * 数据库服务器存放csv文件地址
     */
    private String dbServCsvDir;

    /**
     * 文件主工程服务器URL
     */
    private String remoteUrl;

    /**
     * 文件主工程服务器账户
     */
    private String remoteUser;

    /**
     * 文件主工程服务器密码
     */
    private String remotePassword;

    /**
     * 文件主工程服务器文件存储路径
     */
    private String remoteFileDir;

    /**
     * 审核流启动URL
     */
    private String auditUrl;

    /**
     * 主微服务标识  0:主工程  1:微服务
     */
    private String subMicFlag;

    /**
     * 备用字段1
     */
    private String reserve01;

    /**
     * 备用字段2
     */
    private String reserve02;

    /**
     * 备用字段3
     */
    private String reserve03;

    /**
     * 备用字段4
     */
    private String reserve04;

    /**
     * 备用字段5
     */
    private String reserve05;

    /**
     * 备用字段6
     */
    private String reserve06;

    /**
     * 备用字段7
     */
    private String reserve07;

    /**
     * 备用字段8
     */
    private String reserve08;

    /**
     * 备用字段9
     */
    private String reserve09;

    /**
     * 备用字段10
     */
    private String reserve10;

    /**
     * 删除标识
     */
    private String deleteFlag;

    /**
     * 更新次数
     */
    private Long updateCount;

    /**
     * 作成年月日
     */
    private Date createDate;

    /**
     * 作成用户ID
     */
    private String createUserId;

    /**
     * 更新年月日
     */
    private Date updateDate;

    /**
     * 更新用户ID
     */
    private String updateUserId;

    /**
     * 更新标识
     */
    private String updateFlag;

    /**
     * 当前版本号
     */
    private Long nowVersionNo;

    /**
     * 更新前版本号
     */
    private Long prevVersionNo;

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    public String getId() {
        return id;
    }

    /**
     * 主键ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 同步事务ID
     */
    public String getContentId() {
        return contentId;
    }

    /**
     * 同步事务ID
     */
    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    /**
     * 公司ID
     */
    public String getCompId() {
        return compId;
    }

    /**
     * 公司ID
     */
    public void setCompId(String compId) {
        this.compId = compId;
    }

    /**
     * 数据库url
     */
    public String getDbUrl() {
        return dbUrl;
    }

    /**
     * 数据库url
     */
    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    /**
     * 数据库端口号
     */
    public String getDbPort() {
        return dbPort;
    }

    /**
     * 数据库端口号
     */
    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    /**
     * 数据库名称
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * 数据库名称
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * 数据库账户
     */
    public String getDbUserName() {
        return dbUserName;
    }

    /**
     * 数据库账户
     */
    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    /**
     * 数据库密码
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * 数据库密码
     */
    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    /**
     * 数据库服务器账户
     */
    public String getDbServUserName() {
        return dbServUserName;
    }

    /**
     * 数据库服务器账户
     */
    public void setDbServUserName(String dbServUserName) {
        this.dbServUserName = dbServUserName;
    }

    /**
     * 数据库服务器密码
     */
    public String getDbServPassword() {
        return dbServPassword;
    }

    /**
     * 数据库服务器密码
     */
    public void setDbServPassword(String dbServPassword) {
        this.dbServPassword = dbServPassword;
    }

    /**
     * 数据库服务器存放csv文件地址
     */
    public String getDbServCsvDir() {
        return dbServCsvDir;
    }

    /**
     * 数据库服务器存放csv文件地址
     */
    public void setDbServCsvDir(String dbServCsvDir) {
        this.dbServCsvDir = dbServCsvDir;
    }

    /**
     * 文件主工程服务器URL
     */
    public String getRemoteUrl() {
        return remoteUrl;
    }

    /**
     * 文件主工程服务器URL
     */
    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }

    /**
     * 文件主工程服务器账户
     */
    public String getRemoteUser() {
        return remoteUser;
    }

    /**
     * 文件主工程服务器账户
     */
    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }

    /**
     * 文件主工程服务器密码
     */
    public String getRemotePassword() {
        return remotePassword;
    }

    /**
     * 文件主工程服务器密码
     */
    public void setRemotePassword(String remotePassword) {
        this.remotePassword = remotePassword;
    }

    /**
     * 文件主工程服务器文件存储路径
     */
    public String getRemoteFileDir() {
        return remoteFileDir;
    }

    /**
     * 文件主工程服务器文件存储路径
     */
    public void setRemoteFileDir(String remoteFileDir) {
        this.remoteFileDir = remoteFileDir;
    }

    /**
     * 审核流启动URL
     */
    public String getAuditUrl() {
        return auditUrl;
    }

    /**
     * 审核流启动URL
     */
    public void setAuditUrl(String auditUrl) {
        this.auditUrl = auditUrl;
    }

    /**
     * 主微服务标识  0:主工程  1:微服务
     */
    public String getSubMicFlag() {
        return subMicFlag;
    }

    /**
     * 主微服务标识  0:主工程  1:微服务
     */
    public void setSubMicFlag(String subMicFlag) {
        this.subMicFlag = subMicFlag;
    }

    /**
     * 备用字段1
     */
    public String getReserve01() {
        return reserve01;
    }

    /**
     * 备用字段1
     */
    public void setReserve01(String reserve01) {
        this.reserve01 = reserve01;
    }

    /**
     * 备用字段2
     */
    public String getReserve02() {
        return reserve02;
    }

    /**
     * 备用字段2
     */
    public void setReserve02(String reserve02) {
        this.reserve02 = reserve02;
    }

    /**
     * 备用字段3
     */
    public String getReserve03() {
        return reserve03;
    }

    /**
     * 备用字段3
     */
    public void setReserve03(String reserve03) {
        this.reserve03 = reserve03;
    }

    /**
     * 备用字段4
     */
    public String getReserve04() {
        return reserve04;
    }

    /**
     * 备用字段4
     */
    public void setReserve04(String reserve04) {
        this.reserve04 = reserve04;
    }

    /**
     * 备用字段5
     */
    public String getReserve05() {
        return reserve05;
    }

    /**
     * 备用字段5
     */
    public void setReserve05(String reserve05) {
        this.reserve05 = reserve05;
    }

    /**
     * 备用字段6
     */
    public String getReserve06() {
        return reserve06;
    }

    /**
     * 备用字段6
     */
    public void setReserve06(String reserve06) {
        this.reserve06 = reserve06;
    }

    /**
     * 备用字段7
     */
    public String getReserve07() {
        return reserve07;
    }

    /**
     * 备用字段7
     */
    public void setReserve07(String reserve07) {
        this.reserve07 = reserve07;
    }

    /**
     * 备用字段8
     */
    public String getReserve08() {
        return reserve08;
    }

    /**
     * 备用字段8
     */
    public void setReserve08(String reserve08) {
        this.reserve08 = reserve08;
    }

    /**
     * 备用字段9
     */
    public String getReserve09() {
        return reserve09;
    }

    /**
     * 备用字段9
     */
    public void setReserve09(String reserve09) {
        this.reserve09 = reserve09;
    }

    /**
     * 备用字段10
     */
    public String getReserve10() {
        return reserve10;
    }

    /**
     * 备用字段10
     */
    public void setReserve10(String reserve10) {
        this.reserve10 = reserve10;
    }

    /**
     * 删除标识
     */
    public String getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * 删除标识
     */
    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 更新次数
     */
    public Long getUpdateCount() {
        return updateCount;
    }

    /**
     * 更新次数
     */
    public void setUpdateCount(Long updateCount) {
        this.updateCount = updateCount;
    }

    /**
     * 作成年月日
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * 作成年月日
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * 作成用户ID
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * 作成用户ID
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * 更新年月日
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * 更新年月日
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * 更新用户ID
     */
    public String getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 更新用户ID
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * 更新标识
     */
    public String getUpdateFlag() {
        return updateFlag;
    }

    /**
     * 更新标识
     */
    public void setUpdateFlag(String updateFlag) {
        this.updateFlag = updateFlag;
    }

    /**
     * 当前版本号
     */
    public Long getNowVersionNo() {
        return nowVersionNo;
    }

    /**
     * 当前版本号
     */
    public void setNowVersionNo(Long nowVersionNo) {
        this.nowVersionNo = nowVersionNo;
    }

    /**
     * 更新前版本号
     */
    public Long getPrevVersionNo() {
        return prevVersionNo;
    }

    /**
     * 更新前版本号
     */
    public void setPrevVersionNo(Long prevVersionNo) {
        this.prevVersionNo = prevVersionNo;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TbSyncMicservContent other = (TbSyncMicservContent) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getContentId() == null ? other.getContentId() == null : this.getContentId().equals(other.getContentId()))
            && (this.getCompId() == null ? other.getCompId() == null : this.getCompId().equals(other.getCompId()))
            && (this.getDbUrl() == null ? other.getDbUrl() == null : this.getDbUrl().equals(other.getDbUrl()))
            && (this.getDbPort() == null ? other.getDbPort() == null : this.getDbPort().equals(other.getDbPort()))
            && (this.getDbName() == null ? other.getDbName() == null : this.getDbName().equals(other.getDbName()))
            && (this.getDbUserName() == null ? other.getDbUserName() == null : this.getDbUserName().equals(other.getDbUserName()))
            && (this.getDbPassword() == null ? other.getDbPassword() == null : this.getDbPassword().equals(other.getDbPassword()))
            && (this.getDbServUserName() == null ? other.getDbServUserName() == null : this.getDbServUserName().equals(other.getDbServUserName()))
            && (this.getDbServPassword() == null ? other.getDbServPassword() == null : this.getDbServPassword().equals(other.getDbServPassword()))
            && (this.getDbServCsvDir() == null ? other.getDbServCsvDir() == null : this.getDbServCsvDir().equals(other.getDbServCsvDir()))
            && (this.getRemoteUrl() == null ? other.getRemoteUrl() == null : this.getRemoteUrl().equals(other.getRemoteUrl()))
            && (this.getRemoteUser() == null ? other.getRemoteUser() == null : this.getRemoteUser().equals(other.getRemoteUser()))
            && (this.getRemotePassword() == null ? other.getRemotePassword() == null : this.getRemotePassword().equals(other.getRemotePassword()))
            && (this.getRemoteFileDir() == null ? other.getRemoteFileDir() == null : this.getRemoteFileDir().equals(other.getRemoteFileDir()))
            && (this.getAuditUrl() == null ? other.getAuditUrl() == null : this.getAuditUrl().equals(other.getAuditUrl()))
            && (this.getSubMicFlag() == null ? other.getSubMicFlag() == null : this.getSubMicFlag().equals(other.getSubMicFlag()))
            && (this.getReserve01() == null ? other.getReserve01() == null : this.getReserve01().equals(other.getReserve01()))
            && (this.getReserve02() == null ? other.getReserve02() == null : this.getReserve02().equals(other.getReserve02()))
            && (this.getReserve03() == null ? other.getReserve03() == null : this.getReserve03().equals(other.getReserve03()))
            && (this.getReserve04() == null ? other.getReserve04() == null : this.getReserve04().equals(other.getReserve04()))
            && (this.getReserve05() == null ? other.getReserve05() == null : this.getReserve05().equals(other.getReserve05()))
            && (this.getReserve06() == null ? other.getReserve06() == null : this.getReserve06().equals(other.getReserve06()))
            && (this.getReserve07() == null ? other.getReserve07() == null : this.getReserve07().equals(other.getReserve07()))
            && (this.getReserve08() == null ? other.getReserve08() == null : this.getReserve08().equals(other.getReserve08()))
            && (this.getReserve09() == null ? other.getReserve09() == null : this.getReserve09().equals(other.getReserve09()))
            && (this.getReserve10() == null ? other.getReserve10() == null : this.getReserve10().equals(other.getReserve10()))
            && (this.getDeleteFlag() == null ? other.getDeleteFlag() == null : this.getDeleteFlag().equals(other.getDeleteFlag()))
            && (this.getUpdateCount() == null ? other.getUpdateCount() == null : this.getUpdateCount().equals(other.getUpdateCount()))
            && (this.getCreateDate() == null ? other.getCreateDate() == null : this.getCreateDate().equals(other.getCreateDate()))
            && (this.getCreateUserId() == null ? other.getCreateUserId() == null : this.getCreateUserId().equals(other.getCreateUserId()))
            && (this.getUpdateDate() == null ? other.getUpdateDate() == null : this.getUpdateDate().equals(other.getUpdateDate()))
            && (this.getUpdateUserId() == null ? other.getUpdateUserId() == null : this.getUpdateUserId().equals(other.getUpdateUserId()))
            && (this.getUpdateFlag() == null ? other.getUpdateFlag() == null : this.getUpdateFlag().equals(other.getUpdateFlag()))
            && (this.getNowVersionNo() == null ? other.getNowVersionNo() == null : this.getNowVersionNo().equals(other.getNowVersionNo()))
            && (this.getPrevVersionNo() == null ? other.getPrevVersionNo() == null : this.getPrevVersionNo().equals(other.getPrevVersionNo()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getContentId() == null) ? 0 : getContentId().hashCode());
        result = prime * result + ((getCompId() == null) ? 0 : getCompId().hashCode());
        result = prime * result + ((getDbUrl() == null) ? 0 : getDbUrl().hashCode());
        result = prime * result + ((getDbPort() == null) ? 0 : getDbPort().hashCode());
        result = prime * result + ((getDbName() == null) ? 0 : getDbName().hashCode());
        result = prime * result + ((getDbUserName() == null) ? 0 : getDbUserName().hashCode());
        result = prime * result + ((getDbPassword() == null) ? 0 : getDbPassword().hashCode());
        result = prime * result + ((getDbServUserName() == null) ? 0 : getDbServUserName().hashCode());
        result = prime * result + ((getDbServPassword() == null) ? 0 : getDbServPassword().hashCode());
        result = prime * result + ((getDbServCsvDir() == null) ? 0 : getDbServCsvDir().hashCode());
        result = prime * result + ((getRemoteUrl() == null) ? 0 : getRemoteUrl().hashCode());
        result = prime * result + ((getRemoteUser() == null) ? 0 : getRemoteUser().hashCode());
        result = prime * result + ((getRemotePassword() == null) ? 0 : getRemotePassword().hashCode());
        result = prime * result + ((getRemoteFileDir() == null) ? 0 : getRemoteFileDir().hashCode());
        result = prime * result + ((getAuditUrl() == null) ? 0 : getAuditUrl().hashCode());
        result = prime * result + ((getSubMicFlag() == null) ? 0 : getSubMicFlag().hashCode());
        result = prime * result + ((getReserve01() == null) ? 0 : getReserve01().hashCode());
        result = prime * result + ((getReserve02() == null) ? 0 : getReserve02().hashCode());
        result = prime * result + ((getReserve03() == null) ? 0 : getReserve03().hashCode());
        result = prime * result + ((getReserve04() == null) ? 0 : getReserve04().hashCode());
        result = prime * result + ((getReserve05() == null) ? 0 : getReserve05().hashCode());
        result = prime * result + ((getReserve06() == null) ? 0 : getReserve06().hashCode());
        result = prime * result + ((getReserve07() == null) ? 0 : getReserve07().hashCode());
        result = prime * result + ((getReserve08() == null) ? 0 : getReserve08().hashCode());
        result = prime * result + ((getReserve09() == null) ? 0 : getReserve09().hashCode());
        result = prime * result + ((getReserve10() == null) ? 0 : getReserve10().hashCode());
        result = prime * result + ((getDeleteFlag() == null) ? 0 : getDeleteFlag().hashCode());
        result = prime * result + ((getUpdateCount() == null) ? 0 : getUpdateCount().hashCode());
        result = prime * result + ((getCreateDate() == null) ? 0 : getCreateDate().hashCode());
        result = prime * result + ((getCreateUserId() == null) ? 0 : getCreateUserId().hashCode());
        result = prime * result + ((getUpdateDate() == null) ? 0 : getUpdateDate().hashCode());
        result = prime * result + ((getUpdateUserId() == null) ? 0 : getUpdateUserId().hashCode());
        result = prime * result + ((getUpdateFlag() == null) ? 0 : getUpdateFlag().hashCode());
        result = prime * result + ((getNowVersionNo() == null) ? 0 : getNowVersionNo().hashCode());
        result = prime * result + ((getPrevVersionNo() == null) ? 0 : getPrevVersionNo().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", contentId=").append(contentId);
        sb.append(", compId=").append(compId);
        sb.append(", dbUrl=").append(dbUrl);
        sb.append(", dbPort=").append(dbPort);
        sb.append(", dbName=").append(dbName);
        sb.append(", dbUserName=").append(dbUserName);
        sb.append(", dbPassword=").append(dbPassword);
        sb.append(", dbServUserName=").append(dbServUserName);
        sb.append(", dbServPassword=").append(dbServPassword);
        sb.append(", dbServCsvDir=").append(dbServCsvDir);
        sb.append(", remoteUrl=").append(remoteUrl);
        sb.append(", remoteUser=").append(remoteUser);
        sb.append(", remotePassword=").append(remotePassword);
        sb.append(", remoteFileDir=").append(remoteFileDir);
        sb.append(", auditUrl=").append(auditUrl);
        sb.append(", subMicFlag=").append(subMicFlag);
        sb.append(", reserve01=").append(reserve01);
        sb.append(", reserve02=").append(reserve02);
        sb.append(", reserve03=").append(reserve03);
        sb.append(", reserve04=").append(reserve04);
        sb.append(", reserve05=").append(reserve05);
        sb.append(", reserve06=").append(reserve06);
        sb.append(", reserve07=").append(reserve07);
        sb.append(", reserve08=").append(reserve08);
        sb.append(", reserve09=").append(reserve09);
        sb.append(", reserve10=").append(reserve10);
        sb.append(", deleteFlag=").append(deleteFlag);
        sb.append(", updateCount=").append(updateCount);
        sb.append(", createDate=").append(createDate);
        sb.append(", createUserId=").append(createUserId);
        sb.append(", updateDate=").append(updateDate);
        sb.append(", updateUserId=").append(updateUserId);
        sb.append(", updateFlag=").append(updateFlag);
        sb.append(", nowVersionNo=").append(nowVersionNo);
        sb.append(", prevVersionNo=").append(prevVersionNo);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}