package com.yh.tb.schema.dao.master;

import com.yh.tb.schema.domain.TbSyncMicservContent;

/**
* @author hbjav
* @description 针对表【tb_sync_micserv_content(记录微服务同步事务配置的内容信息)】的数据库操作Mapper
* @createDate 2023-01-16 16:43:17
* @Entity com.yh.tb.schema.domain.TbSyncMicservContent
*/
public interface TbSyncMicservContentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TbSyncMicservContent record);

    int insertSelective(TbSyncMicservContent record);

    TbSyncMicservContent selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TbSyncMicservContent record);

    int updateByPrimaryKey(TbSyncMicservContent record);

}
