package com.yh.tb.schema.dao.master;

import com.yh.tb.schema.domain.TbSyncContent;

/**
* @author hbjav
* @description 针对表【tb_sync_content(记录需要同步的内容信息)】的数据库操作Mapper
* @createDate 2023-01-16 16:43:17
* @Entity com.yh.tb.schema.domain.TbSyncContent
*/
public interface TbSyncContentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(TbSyncContent record);

    int insertSelective(TbSyncContent record);

    TbSyncContent selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TbSyncContent record);

    int updateByPrimaryKey(TbSyncContent record);

}
